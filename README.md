# About this project

Small project that is useful for a someone that is not allowed to use google analytics but that happens to have an
installation of splunk lying around.

The project is a re-implementation of the blog post 
[still using 3rd party web analytics providers build your own using splunk](http://blogs.splunk.com/2013/10/17/still-using-3rd-party-web-analytics-providers-build-your-own-using-splunk/)
and uses spring-boot as a collector instead of the supplied [node.js](https://github.com/splunk/splunk-demo-collector-for-analyticsjs) 
one.

# Demo

1. Start the backend `cd splunk-as-analytics-collector && mvn spring-boot:run`.
2. Start the demo web application `cd splunk-as-analytics-demo && mvn spring-boot:run`.
3. Open your preferred browser and direct it to (http://localhost:8080/).
4. Watch the console from `splunk-as-analytics-collector` to see that will be written to splunk.

# Appendix: How to use sp.js Analytics JavaScript Library

## Setup 
To use `sp.js`, simply paste the following snippet of code before the closing `</head>` tag on your page:

```html
<script type="text/javascript">
    var sp=sp||[];(function(){var e=["init","identify","track","trackLink","pageview"],t=function(e){return function(){sp.push([e].concat(Array.prototype.slice.call(arguments,0)))}};for(var n=0;n<e.length;n++)sp[e[n]]=t(e[n])})(),sp.load=function(e,o){sp._endpoint=e;if(o){sp.init(o)};var t=document.createElement("script");t.type="text/javascript",t.async=!0,t.src=("https:"===document.location.protocol?"https://":"http://")+"d21ey8j28ejz92.cloudfront.net/analytics/v1/sp.min.js";var n=document.getElementsByTagName("script")[0];n.parentNode.insertBefore(t,n)};
    sp.load("https://www.example.com"); // Replace with your own Collector URL
</script>
```

`sp.js` will provide you with a clean api for tracking, pageviews and more. Please see the [original project](https://github.com/splunk/splunk-demo-collector-for-analyticsjs#appendix-how-to-use-spjs-analytics-javascript-library) 
for a detailed description.

## Please note

This project includes an alternative implementation of `sp.js`called `sp-no-cookies-min.js` that should be used instead of
the cloud hosted one.

# Contributing

The project consists of two modules

* splunk-as-analytics-collector
* splunk-as-analytics-demo

The collector module is the core of the project providing the bridge between `sp.js` and splunk using 
slf4j (logback) file based logging.

The demo module is a very small example demonstration web application that uses `sp.js` and the collector to log user
interactions.