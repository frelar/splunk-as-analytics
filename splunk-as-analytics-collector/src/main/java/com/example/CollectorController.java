package com.example;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.HttpStatus.ACCEPTED;

/**
 * @author Fredrik Larsson (N342100)
 * @since 1.0
 */
@RestController
public class CollectorController {
    private final SplunkForwarderService splunk;

    @Autowired
    public CollectorController(SplunkForwarderService splunk) {
        this.splunk = splunk;
    }

    @CrossOrigin
    @GetMapping("/collector/track")
    @ResponseStatus(ACCEPTED)
    public void collect(@RequestParam("data") String data) {
        splunk.forward(data);
    }
}
