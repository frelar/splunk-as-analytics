package com.example;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author Fredrik Larsson (N342100)
 * @since 1.0
 */
@Slf4j(topic = "splunk-analytics")
@Service
public class SplunkForwarderService {
    void forward(String data) {
        log.info(data);
    }
}
