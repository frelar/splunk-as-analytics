package com.example;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @author Fredrik Larsson (N342100)
 * @since 1.0
 */
@RunWith(SpringRunner.class)
@WebMvcTest(CollectorController.class)
public class TrackingTest {
    @Autowired
    private MockMvc mvc;

    @SpyBean
    private SplunkForwarderService splunk;

    @Test
    public void contextLoads() {
    }

    @Test
    public void basicTrackingShouldWriteToSlf4J() throws Exception {
        this.mvc.perform(get("/collector/track?data=tut"))
                .andExpect(status().isAccepted());

        verify(splunk).forward("tut");
    }
}
